import { getFromLS } from "../utils/storage";
import _ from "lodash";

export default function ThemeSelector(props) {
  const themes = getFromLS("all-themes");

  const handleChange = (name) => {
    const obj = _.find(themes.data, { name: name });
    console.log(themes);
    props.setSelectedTheme(obj);
  };

  return (
    <>
      <select
        id="theme-selector"
        name="theme-selector"
        onChange={(e) => handleChange(e.target.value)}
      >
        {_.map(themes, (theme) => (
          <>
            <option value={theme.light.name} key={theme.light.index}>
              {theme.light.name}
            </option>
            <option value={theme.seaWave.name} key={theme.seaWave.index}>
              {theme.seaWave.name}
            </option>
          </>
        ))}
      </select>
    </>
  );
}
