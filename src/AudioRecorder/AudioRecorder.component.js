import { RecorderButton, StopButton } from "./styles/RecorderButons";

export default function AudioRecorderComponent(props) {
  return (
    <div className="audio-recorder-container">
      <audio>Audio</audio>
      <RecorderButton
        className="record"
        onClick={() => props.onRecordStart()}
        mediaRecorderState={props.mediaRecorderState}
      >
        {props.mediaRecorderState === "recording" ? "Recording" : "Record"}
      </RecorderButton>

      <StopButton
        className="stop"
        onClick={() => props.onRecordStop()}
        mediaRecorderState={props.mediaRecorderState}
      >
        Stop
      </StopButton>

      <ul className="sound-clips">
        {props.recordedAudios.map((audio, index) => {
          return (
            <>
              <li key={index}>
                <h4>{audio.name}</h4>
              </li>
              <audio src={audio.url} controls></audio>
              <button onClick={() => props.onRecordDelete(audio.url)}>
                Delete
              </button>
            </>
          );
        })}
      </ul>
    </div>
  );
}
