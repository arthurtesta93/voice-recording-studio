import styled, { css, keyframes } from "styled-components";

const blinkingAnimation = keyframes`        
0% {
  background-color: #FF0000; 
}
100% {
  background-color: #FFFFFF;
}
`;

const animation = () =>
  css`
    ${blinkingAnimation} 1s infinite;
  `;

export const RecorderButton = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid black;
  color: ${(props) =>
    props.mediaRecorderState === "recording" ? "white" : "black"};
  margin: 0 1em;
  padding: 0.25em 1em;
  cursor: pointer;
  animation: ${(props) =>
    props.mediaRecorderState === "recording" ? animation : null};
`;

export const StopButton = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid black;
  color: ${(props) =>
    props.mediaRecorderState === "recording" ? "black" : "red"};
  margin: 0 1em;
  padding: 0.25em 1em;
  cursor: pointer;
`;
