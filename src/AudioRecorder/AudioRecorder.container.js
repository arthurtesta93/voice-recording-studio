import { useState, useEffect, useRef } from "react";
import ThemeSelector from "../themes/ThemeSelector";
import AudioRecorderComponent from "../AudioRecorder/AudioRecorder.component";

export default function AudioRecorderContainer(props) {
  const [mediaRecorder, setMediaRecorder] = useState({});
  const [userMediaSuccess, setUserMediaSuccess] = useState(false);
  const [chunks, setChunks] = useState([]);
  const [recordedAudios, setRecordedAudios] = useState([]);
  const [mediaRecorderState, setMediaRecorderState] = useState("");

  const recordedAudiosRef = useRef([]);
  recordedAudiosRef.current = recordedAudios;

  useEffect(() => {
    if (!userMediaSuccess) {
      if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        console.log("getUserMedia supported");
        navigator.mediaDevices
          .getUserMedia({
            audio: true,
          })
          .then(function (stream) {
            const newMediaRecorder = new MediaRecorder(stream);
            newMediaRecorder.ondataavailable = function (e) {
              const updatedChunks = chunks;
              updatedChunks.push(e.data);
              setChunks(updatedChunks);
            };
            newMediaRecorder.onstop = function (e) {
              const clipName = prompt("Insira o nome do trecho de áudio:");

              const blob = new Blob(chunks, { type: "audio/wav; codecs=opus" });
              setChunks([]);
              const audioURL = window.URL.createObjectURL(blob);

              const audioObject = {
                url: audioURL,
                blob: blob,
                name: clipName,
              };

              saveRecordedAudio(audioObject);
            };
            setMediaRecorder(newMediaRecorder);
            setUserMediaSuccess(true);
          })

          .catch(function (err) {
            console.log("The following error occurred" + err);
            setUserMediaSuccess(false);
          });
      }
    }
  });

  const saveRecordedAudio = (audio) => {
    const updatedRecordedAudios = [...recordedAudiosRef.current];
    updatedRecordedAudios.push(audio);
    setRecordedAudios(updatedRecordedAudios);
  };

  const onRecordStart = () => {
    if (userMediaSuccess && mediaRecorder.state === "inactive") {
      mediaRecorder.start();
      setMediaRecorderState(mediaRecorder.state);
    }
  };

  const onRecordStop = () => {
    mediaRecorder.stop();
    setMediaRecorderState(mediaRecorder.state);
  };

  const onRecordDelete = (audioURL) => {
    const updatedRecordedAudios = recordedAudios;
    const recordToDeleteIndex = updatedRecordedAudios.findIndex(
      (i) => i.url === audioURL
    );
    updatedRecordedAudios.splice(recordToDeleteIndex, 1);
    setRecordedAudios(updatedRecordedAudios);
  };

  return (
    <>
      <AudioRecorderComponent
        onRecordDelete={onRecordDelete}
        onRecordStop={onRecordStop}
        onRecordStart={onRecordStart}
        recordedAudios={recordedAudios}
        mediaRecorderState={mediaRecorderState}
      />

      <ThemeSelector setSelectedTheme={props.setSelectedTheme} />
    </>
  );
}
