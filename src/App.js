import { useState, useEffect } from "react";
import AudioRecorderContainer from "./AudioRecorder/AudioRecorder.container";
import { GlobalStyles } from "./themes/GlobalStyles";
import styled, { ThemeProvider } from "styled-components";
import WebFont from "webfontloader";
import { useTheme } from "./themes/useTheme";
import "./App.css";

const Container = styled.div`
  margin: 5px auto 5px auto;
`;

function App() {
  const { theme, themeLoaded, getFonts } = useTheme();
  const [selectedTheme, setSelectedTheme] = useState(theme);

  useEffect(() => {
    setSelectedTheme(theme);
  }, [themeLoaded, theme]);

  useEffect(() => {
    WebFont.load({
      google: {
        families: getFonts(),
      },
    });
  });

  return (
    <>
      {themeLoaded && (
        <ThemeProvider theme={selectedTheme}>
          <GlobalStyles />
          <Container style={{ fontFamily: selectedTheme.font }}>
            <AudioRecorderContainer setSelectedTheme={setSelectedTheme} />
          </Container>
        </ThemeProvider>
      )}
    </>
  );
}

export default App;
