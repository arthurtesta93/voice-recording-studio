# Voice Recording Studio

This is an educational project with the objective of showcasing the use of the MediaRecorder API within a modern React application boostrapped with Create React App. Since most of the available documentation on the MediaRecorder API makes use of either vanilla JavaScript injected into a plain html project, or an out-of-the-box React component with little flexibility for customization and props manipulation, this is a simple study into the ground-up implementation of a voice recorder in a React application using hooks, functional components and modern practices of React front-end architecture.

# Stack and functionalities

This application uses some of the default hooks within the React API (useEffect, useState and useRef). The main component 'AudioRecorder' does not use any third party library/component, other than the aforementioned React API and MediaRecorder API, meaning that the AudioRecorder component itself can be built from any basic bootstrapped React project.

Other used dependencies include:

- styled-components (https://styled-components.com): Styling and UI design
- webfontloader (https://www.npmjs.com/package/webfontloader): Font source
- lodash (https://lodash.com/): Javascript utility for easier object/array iteration, manipulation, etc.
- DnD kit (https://dndkit.com/): Drag and Drop components made easy!
